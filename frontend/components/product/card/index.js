import React from 'react'
import { useState } from 'react'

import { Product } from './styles'

export default (props) => {
  const [count, setCount] = useState(0)

  return (
    <Product onClick={() => setCount(count + 1)}>
      <span class="product-name">{props.name}</span><span>{count}</span>
    </Product>
  )
}