import styled from 'styled-components'

const Product = styled.div`
  cursor: pointer;
`

export {
  Product
}