import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Product from '../components/product/card'

configure({adapter: new Adapter()})

test('Product card test', () => {
  const wrapper = shallow(<Product name="Foo" />);
  expect(wrapper.contains(<span class="product-name">Foo</span>)).toEqual(true)
})