npm install
if [ $ENV = dev ]
then
  echo 'ENVIRONMENT: develop'
  node index.js
else
  echo 'ENVIRONMENT: production'
  node index.js
fi