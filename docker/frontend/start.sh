npm install
if [ $ENV = dev ]
then
  echo 'ENVIRONMENT: develop'
  npm run dev
else
  echo 'ENVIRONMENT: production'
  npm run build
  npm run start
fi