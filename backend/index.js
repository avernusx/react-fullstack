var keystone = require('keystone');

console.log('MONGO_URI', process.env.MONGO_URI)

keystone.init({
  'cookie secret': '43gh94rvn9rn9',
  'mongo': process.env.MONGO_URI || "mongodb://database:27017/database"
});

keystone.start();